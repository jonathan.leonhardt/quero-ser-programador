# **Por que eu acho que seria um bom programador?**

**Segue uma lista de pontos que podem responder a pergunta:**

- Gosto de resolver problemas.
- Gosto de pensar logicamente uma solução aos problemas.
- Me interesso *muito* sobre desenvolvimento web.
- Normalmente busco por respostas sozinho.
- Meu objetivo atualmente é obter conhecimento com desenvolvimento.

----------

**Além disso, estudo análise e desenvolvimento de sistema e busco fortemente oportunidade de aprender a parte prática de programação, análise e desenvolvimento.**
